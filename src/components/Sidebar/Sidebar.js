import React from 'react';
import Jokes from '../Jokes/Jokes';
import Logo from '../Logo/Logo';
import Navigation from '../Navigation/Navigation';

import classes from './Sidebar.module.css';

const Sidebar = (props) => (
  <div className={classes.Sidebar}>
    <Logo />
    <Navigation />
    <h2>Last 5 jokes</h2>
    <Jokes jokes={props.jokes} />
  </div>
);


export default Sidebar;