import React from 'react';

import classes from './Logo.module.css';
import logo from '../../images/chucknorris.png';

const Logo = () => (
  <div className={classes.Logo}>
    <img src={logo} alt="Jokes" />
  </div>
);

export default Logo;
