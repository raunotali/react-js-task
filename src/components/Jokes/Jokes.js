import React from 'react';
import Joke from './Joke';

import classes from './Jokes.module.css';

const Jokes = (props) => (
  <div className={classes.Jokes}>
    <ul>
      {props.jokes.map((joke, index) => {
        return (
          <li key={index}>
              <Joke joke={joke} />
          </li>
        );
      })}
    </ul>
  </div>
);

export default Jokes;