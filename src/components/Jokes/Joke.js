import React from 'react';

import classes from './Joke.module.css';

const Joke = (props) => (
  <p className={classes.Joke}>
    "{props.joke}"
  </p>
);

export default Joke;