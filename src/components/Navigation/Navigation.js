import React from 'react';
import { NavLink } from 'react-router-dom';

import classes from './Navigation.module.css';

const Navigation = () => (
  <div className={classes.Navigation}>
    <p className={classes.Heading}>Menu</p>
    <ul>
      <li><NavLink to="/" exact>Home</NavLink></li>
      <li><NavLink to="/article" exact>Article</NavLink></li>
    </ul>
  </div>
);

export default Navigation;