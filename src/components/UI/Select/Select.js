import React from 'react';

const Select = (props) => {
  return (
    <>
      <label htmlFor="categories">Choose a joke:</label>
      <select onChange={props.onchange} name="categories" id="categories">
        {props.categories.map((category, index) => {
          return <option key={index} value={category}>{category.toUpperCase()}</option>;
        })}
      </select>
    </>
  );
}

export default Select;