import React, { useEffect, useState } from 'react';

import Joke from '../components/Jokes/Joke';
import Select from '../components/UI/Select/Select';

const Home = (props) => {

  const [categories, setCategories] = useState([]);
  const [joke, setJoke] = useState({});

  useEffect(() => {
    fetch("https://api.chucknorris.io/jokes/categories")
      .then(res => res.json())
      .then(
        (result) => {
          setCategories(result);
        });
  }, []);

  const dropdownSelectHandler = ((event) => {
    let category = event.target.value;
    fetch(`https://api.chucknorris.io/jokes/random?category=${category}`)
      .then(res => res.json())
      .then(
        (result) => {
          setJoke(result);
          updateAllJokes(result);
        });
  });

  const randomJokeHandler = () => {
    fetch("https://api.chucknorris.io/jokes/random")
      .then(res => res.json())
      .then(
        (result) => {
          setJoke(result);
          updateAllJokes(result);
        });
  }

  const updateAllJokes = (joke) => {
    props.updateJokes(joke.value);
  };

  return (
    <div className="pages home">
      <h1 className="heading">Home</h1>
      <h2>Choose a category or press the button to retrive a random joke</h2>
      <Select onchange={dropdownSelectHandler} categories={categories} />
      <button onClick={randomJokeHandler}>Random</button>
      <div className="joke main">
        {joke.value && <Joke joke={joke.value} />}
      </div>
    </div>
  );
};

export default Home;