import React from 'react';

const Article = () => (
  <div className="pages article">
    <h1>Article</h1>
    <img style={{maxWidth: "50%", heigth: "100%"}} src="https://i.pinimg.com/originals/aa/d9/0d/aad90db6a21a9015bcbb9d97dec3dd69.jpg" alt="Chuck Norris " />
    <p><strong>Carlos Ray "Chuck" Norris</strong> (born March 10, 1940) is an American martial artist, actor, film producer, and screenwriter. After serving in the United States Air Force, Norris won many martial arts championships and later founded his own discipline Chun Kuk Do. Norris is a black belt in Tang Soo Do, Brazilian jiu jitsu and judo.[2] Shortly after, in Hollywood, Norris trained celebrities in martial arts. Norris went on to appear in a minor role in the spy film The Wrecking Crew (1969). Friend and fellow actor Bruce Lee invited him to play one of the main villains in Way of the Dragon (1972). While Norris continued acting, friend and student Steve McQueen suggested to him that he take it seriously. Norris took the starring role in the action film Breaker! Breaker! (1977), which turned a profit. His second lead Good Guys Wear Black (1978) became a hit, and Norris became a popular action film star.</p>
  </div>
);

export default Article;