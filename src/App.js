import React, { useState } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import './App.css';
import Layout from './components/Layout/Layout';
import SideBar from './components/Sidebar/Sidebar';
import Home from './pages/Home';
import Article from './pages/Article';
import NotFound from './pages/NotFound';

function App() {

  const [jokes, setJokes] = useState([]);

  const updateJokesHandler = (joke) => {
    setJokes(prevJokes => {
      if (prevJokes.length > 4) {
        prevJokes.pop();
      }
      return [joke, ...prevJokes];
    })
  };

  return (
    <div className="App">
      <BrowserRouter>
        <Layout>
          <SideBar jokes={jokes} />
          <Switch>
            <Route path="/" exact>
              <Home jokes={jokes} updateJokes={updateJokesHandler} />
            </Route>
            <Route path="/article" exact>
              <Article />
            </Route>
            <Route path="*">
              <NotFound />
            </Route>
          </Switch>
        </Layout>
      </BrowserRouter>
    </div>
  );
}

export default App;
